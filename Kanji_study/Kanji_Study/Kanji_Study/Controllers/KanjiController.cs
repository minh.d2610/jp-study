﻿using System.Net.Http;
using System.Threading.Tasks;
using Kanji_Study.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Linq;
using System;

namespace Kanji_Study.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KanjiController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult Get()
        {
            HttpContext.Session.Remove("previewList");
            ReadKanjiService readKanjiService = new ReadKanjiService();
            HttpContext.Session.SetInt32("countRow", readKanjiService.CountKanjiCharacter());
            return Ok(JsonConvert.SerializeObject("done"));
        }

        [HttpGet]
        [Route("RandomCharacter")]
        public ActionResult RandomCharacter()
        {
            ReadKanjiService readKanjiService = new ReadKanjiService();

            int total = 0;
            if (HttpContext.Session.GetInt32("countRow") != null)
            {
                total = HttpContext.Session.GetInt32("countRow").Value;
            }
            else
            {
                total = readKanjiService.CountKanjiCharacter();
            }

            Random rd = new Random();
            int index = rd.Next(2, total);

            var result = readKanjiService.GetRandomCharacter(index);

            if (HttpContext.Session.GetString("previewList") == null || HttpContext.Session.GetString("previewList") == "")
            {
                HttpContext.Session.SetString("previewList", result.index.ToString());
            }
            else
            {
                var preList = HttpContext.Session.GetString("previewList").Split(',');
                if (preList.Length == 10)
                {
                    preList = preList.Where((val, idx) => idx != 0).ToArray();

                }
                HttpContext.Session.SetString("previewList", string.Join(',', preList) + "," + result.index.ToString());
            }
            result.previewList = HttpContext.Session.GetString("previewList");
            return Ok(JsonConvert.SerializeObject(result));
        }

        [HttpGet]
        [Route("PreviewCharacter")]
        public ActionResult PreviewCharacter()
        {
            ReadKanjiService readKanjiService = new ReadKanjiService();
            string[] preList;
            if (HttpContext.Session.GetString("previewList") == null || HttpContext.Session.GetString("previewList") == "")
            {
                return NotFound();
            }
            else
            {
                preList = HttpContext.Session.GetString("previewList").Split(',');
            }
            preList = preList.Where((val, idx) => idx != preList.Length - 1).ToArray();
            var result = readKanjiService.GetRandomCharacter(Convert.ToInt32(preList.Last()));

            if (preList == null)
            {
                HttpContext.Session.SetString("previewList", null);
            }
            else
            {
                HttpContext.Session.SetString("previewList", string.Join(',', preList));
            }
            result.previewList = HttpContext.Session.GetString("previewList");
            return Ok(JsonConvert.SerializeObject(result));
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
