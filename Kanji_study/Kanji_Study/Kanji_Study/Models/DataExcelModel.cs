﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kanji_Study.Models
{
    public class DataExcelModel
    {
        public int index { get; set; }
        public string vnChar { get; set; }
        public string kanjiChar { get; set; }
        public string jpChar { get; set; }
        public string previewList { get; set; }
    }
}
