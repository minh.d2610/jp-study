﻿using Kanji_Study.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kanji_Study.Services
{
    public class ReadKanjiService
    {
        public int CountKanjiCharacter()
        {
            return CommonService.CountRowOfFile();
        }
        public DataExcelModel GetRandomCharacter(int index)
        {
            return CommonService.ReadExcelFileByIndex(index);
        }
    }
}
