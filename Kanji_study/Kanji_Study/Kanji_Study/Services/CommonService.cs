﻿using Kanji_Study.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Kanji_Study.Services
{
    public class CommonService
    {
        public static string GetPathConfig()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@".\Config.xml");
            XmlNode node = doc.DocumentElement.SelectSingleNode("/ImportPath");
            return node.InnerText;
        }

        public static bool SavePathConfig(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return false;
            }
            XmlDocument doc = new XmlDocument();
            doc.Load(@".\Config.xml");
            doc.SelectSingleNode("/ImportPath").InnerText = path;
            doc.Save(@".\Config.xml");
            return true;
        }

        public static int CountRowOfFile()
        {
            var filePath = GetPathConfig();
            if (string.IsNullOrEmpty(filePath))
            {
                return 0;
            }
            else
            {
                int rowCount = 0;
                FileInfo file = new FileInfo(filePath);
                List<DataExcelModel> result = new List<DataExcelModel>();
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    StringBuilder sb = new StringBuilder();
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                    rowCount = worksheet.Dimension.Rows;
                }
                return rowCount;
            }
        }

        public static List<DataExcelModel> ReadExcelFile(int template)
        {
            var filePath = GetPathConfig();
            if (string.IsNullOrEmpty(filePath))
            {
                return null;
            }
            else
            {
                FileInfo file = new FileInfo(filePath);
                List<DataExcelModel> result = new List<DataExcelModel>();
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    StringBuilder sb = new StringBuilder();
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                    int rowCount = worksheet.Dimension.Rows;

                    for (int row = 2; row <= rowCount; row++)
                    {
                        DataExcelModel item = new DataExcelModel();
                        item.index = row;
                        if (worksheet.Cells[row, 1] != null && worksheet.Cells[row, 1].Value != null)
                        {
                            item.jpChar = worksheet.Cells[row, 1].Value.ToString();
                        }
                        if (worksheet.Cells[row, 2] != null && worksheet.Cells[row, 2].Value != null)
                        {
                            item.kanjiChar = worksheet.Cells[row, 2].Value.ToString();
                        }
                        if (worksheet.Cells[row, 3] != null && worksheet.Cells[row, 3].Value != null)
                        {
                            item.vnChar = worksheet.Cells[row, 3].Value.ToString();
                        }
                        result.Add(item);
                    }
                }
                return result;
            }
        }

        public static DataExcelModel ReadExcelFileByIndex(int index)
        {
            var filePath = GetPathConfig();
            if (string.IsNullOrEmpty(filePath) || index <= 1)
            {
                return null;
            }
            else
            {
                DataExcelModel result = new DataExcelModel();
                FileInfo file = new FileInfo(filePath);
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    StringBuilder sb = new StringBuilder();
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                    int rowCount = worksheet.Dimension.Rows;

                    result.index = index;
                    if (worksheet.Cells[index, 1] != null && worksheet.Cells[index, 1].Value != null)
                    {
                        result.jpChar = worksheet.Cells[index, 1].Value.ToString();
                    }
                    else
                    {
                        result.jpChar = string.Empty;
                    }

                    if (worksheet.Cells[index, 2] != null && worksheet.Cells[index, 2].Value != null)
                    {
                        result.kanjiChar = worksheet.Cells[index, 2].Value.ToString();
                    }
                    else
                    {
                        result.kanjiChar = string.Empty;
                    }

                    if (worksheet.Cells[index, 3] != null && worksheet.Cells[index, 3].Value != null)
                    {
                        result.vnChar = worksheet.Cells[index, 3].Value.ToString();
                    }
                    else
                    {
                        result.vnChar = string.Empty;
                    }
                }
                return result;
            }
        }
    }
}